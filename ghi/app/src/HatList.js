import React from "react";

class  HatList extends  React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hats:[],
            isLoaded: false,
        };
    }
    async getHats() {
        const response = await fetch('http://localhost:8090/api/hats')
        if (response.ok) {
            const data = await response.json();
            const hats = data.hats;
            this.setState({
                hats: hats,
                isLoaded: true,
            });
        }
    }

    componentDidMount() {
        this.getHats()
    }

    async handleDelete(event) {
        const deleteUrl = 'http://localhost:8090/api/hats/${event}'
        await fetch(deleteUrl, { method: "DELETE"});
        this.getHats()
    }

    render(){
        const {isLoaded, hats } = this.state;
        if (!isLoaded) {
            return (
                <div>
                    <h1>Hats are being loaded...</h1>
                </div>
            )
        }

        return (
            <table className = "table">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                        return (
                            <tr key = {hat.href}>
                                <td>{ hat.fabric }</td>
                                <td>{ hat.style_name }</td>
                                <td>{ hat.color }</td>
                                <td>{ hat.picture_url}</td>
                                <td>{ hat.location }</td>
                                <td><button onClick={() => this.handleDelete(hat.id)} type="button" className="btn btn-danger">Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
    }
}
export default HatList;
