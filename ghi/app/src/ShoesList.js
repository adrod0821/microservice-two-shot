import React from "react";


class ShoesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shoes: [],
            isLoaded: false
        };
    }

    async getShoes() {
        const response = await fetch('http://localhost:8080/api/shoes')
        if (response.ok) {
            const data = await response.json();
            const shoes = data.shoes;
            this.setState({
                shoes: shoes,
                isLoaded: true,
            });
        }
    }


    componentDidMount() {
        this.getShoes()
    }

    async handleDelete(event) {
        // console.log(event)
        const deleteUrl = `http://localhost:8080/api/shoes/${event}`
        // console.log(deleteUrl)
        await fetch(deleteUrl, { method: "DELETE" });
        this.getShoes()
    }


    render() {
        const { isLoaded, shoes } = this.state;
        if (!isLoaded) {
            return (
                <div>
                    <h1>Shoes are being loaded...</h1>
                </div>
            )
        }

        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Bin</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={ shoe.id }>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.model_name }</td>
                                <td>{ shoe.color }</td>
                                <td>{ shoe.picture_url }</td>
                                <td>{ shoe.bin }</td>
                                <td><button onClick={() => this.handleDelete(shoe.id)} type="button" className="btn btn-danger">Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )
    }
}


export default ShoesList;
