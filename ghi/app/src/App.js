import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatList from './HatList';
import Nav from './Nav';
import ShoesList from './ShoesList';
import NewShoe from './NewShoe';
import NewHat from './NewHat';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path ="hats/" element={<HatList/>} />
          <Route path="shoes/" element={<ShoesList/>} />
          <Route path="shoes/new" element={<NewShoe/>}>
          <Route path="hats/new" element = {<NewHat/>} />

          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
