import React from "react";

class NewShoe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bin: '',
            bins: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangeModel = this.handleChangeModel.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangeURL = this.handleChangeUrl.bind(this)
        this.handleChangeBin = this.handleChangeBin.bind(this)


    }



    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins })
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        delete data.bins;

        const shoesUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
            event.target.reset();
            // this.setState({
            //     manufacturer: '',
            //     model_name: '',
            //     color: '',
            //     picture_url: '',
            //     bin: '',
            // });
        }
    }


    handleChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value })
    }

    handleChangeModel(event) {
        const value = event.target.value;
        this.setState({ model_name: value })
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handleChangeUrl(event) {
        const value = event.target.value;
        this.setState({ picture_url: value })
    }

    handleChangeBin(event) {
        const value = event.target.value;
        this.setState({ bin: value })
    }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new shoe</h1>
                    <form onSubmit={this.handleSubmit} id="add-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                        <label htmlFor="name">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeModel} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" />
                        <label htmlFor="starts">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="ends">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleChangeUrl} placeholder="Picture URL" type="text" name="picture_url" id="picture_url" className="form-control" />
                        <label htmlFor="ends">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handleChangeBin} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a bin</option>
                        {this.state.bins.map(bin => {
                            return (
                            <option key={bin.id} value={bin.id}>{bin.bin_number}</option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        )
    }

}

export default NewShoe
