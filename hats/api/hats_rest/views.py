from django.shortcuts import render
from .models import Hat, LocationVO
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",]
    def get_extra_data(self, o):
        return {"location": o.location.id}

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "shelf_number",
        ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]

    encoders = {
        "location": LocationVODetailEncoder()
    }

@require_http_methods(["GET", "POST"])
def list_hats(request,):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats":hats},
            encoder = HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:

            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status =400
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False
        )
@require_http_methods(["GET", "DELETE"])
def show_hat(request,pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False)
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist: # delete
            return JsonResponse({"message": "Does not exist"})
