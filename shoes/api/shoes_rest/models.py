from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    """
    BinVO instances that gets polled as possible bin options for Shoe instances.
    """
    import_href = models.CharField(max_length=200, unique=True)
    bin_number = models.PositiveIntegerField(null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f'{self.manufacturer} {self.model_name}'

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})
