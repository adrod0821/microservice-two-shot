from django.urls import path

from .views import api_list_shoes, api_show_shoe


urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/new", api_list_shoes, name="api_new_shoe"),
    path("shoes/<int:pk>/", api_show_shoe, name="api_delete_shoe")
]
