from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

# Create your views here.


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "bin_number"]


class ShoeListEncoder(ModelEncoder):
    model= Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color"
    ]

    def get_extra_data(self, o):
        return {"bin": o.bin.id}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {
        "bin": BinVODetailEncoder()
    }



@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            print(content)
            bin = BinVO.objects.get(bin_number=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE","GET"])
def api_show_shoe(request, pk):
    """
    Returns details of an instance of a Shoe Model specified by pk requested.

    It's a dictionary with the specified DetailEncoder parameters as seen above.
    The Bin variable is a dictionary with the specific bin's id and href URL.
    """
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"})
    else:
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"})
